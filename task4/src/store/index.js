import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    driverCat: '11111',
    trueEmplCenter: 238760,
    "handbook": [
        {
            "id": 10,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 100050996,
                "address": null,
                "emplCenterId": 238710,
                "name": "Агентство занятости населения Василеостровского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Василеостровского р-на",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 20,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 210021736,
                "address": null,
                "emplCenterId": 238720,
                "name": "Агентство занятости населения Выборгского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Выборгского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 40,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 410015201,
                "address": null,
                "emplCenterId": 238740,
                "name": "Агентство занятости населения Приморского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Приморского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 50,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 510023615,
                "address": null,
                "emplCenterId": 238750,
                "name": "Агентство занятости населения Калининского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Калининского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 60,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 610020005,
                "address": null,
                "emplCenterId": 238760,
                "name": "Агентство занятости населения Кировского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Кировского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 70,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 700013325,
                "address": null,
                "emplCenterId": 238770,
                "name": "Агентство занятости населения Колпинского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Колпинского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 100,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1000015941,
                "address": null,
                "emplCenterId": 2387100,
                "name": "Агентство занятости населения Московского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Московского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 110,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1110022307,
                "address": null,
                "emplCenterId": 2387110,
                "name": "Агентство занятости населения Невского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Невского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 120,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1220008205,
                "address": null,
                "emplCenterId": 2387120,
                "name": "Агентство занятости населения Адмиралтейского района Санкт-Петербурга",
                "changeDate": "2014-11-04T21:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Адмиралтейского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 130,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 1310000000,
                "userOwnerId": 9990000000,
                "companyId": 410015121,
                "address": null,
                "emplCenterId": 2387130,
                "name": "Агентство занятости населения Петроградского района Санкт-Петербурга",
                "changeDate": "2013-01-10T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Петроградского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 140,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1400007441,
                "address": null,
                "emplCenterId": 2387140,
                "name": "Агентство занятости населения Петродворцового района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Петродворцового района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 150,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1500010263,
                "address": null,
                "emplCenterId": 2387150,
                "name": "Агентство занятости населения Пушкинского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Пушкинского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 160,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1610006281,
                "address": null,
                "emplCenterId": 2387160,
                "name": "Агентство занятости населения Курортного района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Курортного района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 170,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1730053521,
                "address": null,
                "emplCenterId": 2387170,
                "name": "Агентство занятости населения Центрального района  Санкт-Петербурга",
                "changeDate": "2014-11-04T21:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Центрального района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 180,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1800028345,
                "address": null,
                "emplCenterId": 2387180,
                "name": "Агентство занятости населения Фрунзенского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Фрунзенского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 190,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 1900003621,
                "address": null,
                "emplCenterId": 2387190,
                "name": "Агентство занятости населения Кронштадтского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Кронштадтского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 200,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 2010015724,
                "address": null,
                "emplCenterId": 2387200,
                "name": "Агентство занятости населения Красносельского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Красносельского района",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 210,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": 2130016705,
                "address": null,
                "emplCenterId": 2387210,
                "name": "Агентство занятости населения Красногвардейского района Санкт-Петербурга",
                "changeDate": "2013-01-08T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "АЗН Красногвардейского р-н",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 230,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9000000000,
                "userOwnerId": 9990000000,
                "companyId": 2300008883,
                "address": null,
                "emplCenterId": 2387230,
                "name": "Агентство занятости населения города Ломоносова",
                "changeDate": "2013-01-29T20:00:00.000+0000",
                "activitySign": 0,
                "shortName": "АЗН Ломоносова",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 999,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 8880000000,
                "userOwnerId": 9990000000,
                "companyId": 100458,
                "address": null,
                "emplCenterId": 2387999,
                "name": "Комитет по труду и занятости населения Санкт-Петербурга",
                "changeDate": "2011-11-29T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "КТЗН Санкт-Петербурга",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 1
            }
        },
        {
            "id": 900,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 8880000000,
                "userOwnerId": 9990000000,
                "companyId": 9950023681,
                "address": null,
                "emplCenterId": 2387900,
                "name": "Санкт-Петербургское государственное автономное учреждение \"Центр занятости населения Санкт-Петербурга\"",
                "changeDate": "2012-12-11T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "СПб ГАУ ЦЗН",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        },
        {
            "id": 910,
            "classCode": "emplCenter",
            "title": null,
            "data": {
                "userEditorId": 9990000000,
                "userOwnerId": 9990000000,
                "companyId": null,
                "address": null,
                "emplCenterId": 2387910,
                "name": "Санкт-Петербургское государственное автономное учреждение \"Центр трудовых ресурсов\"",
                "changeDate": "2013-07-14T20:00:00.000+0000",
                "activitySign": 1,
                "shortName": "СПб ГАУ ЦТР",
                "creationDate": "2018-07-18T12:44:40.000+0000",
                "parentId": 2
            }
        }
    ]
  },
  mutations: {
    newDriverCat(state, payload) {
      state.driverCat = payload;
    }
  },
  actions: {

  }
})
