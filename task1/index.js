const link = document.querySelector('#link');
const writeClicks = clicks => {
  localStorage.setItem('clicks', clicks);
  document.cookie = `clicks=${clicks}`; // сессионная cookie, доступная в /
}

let clicks;

// При загрузке документа проверяется существует ли
// ключ `clicks` в localStorage. Если нет, таковой создается.
// Далее, в любом случае считывается значение ассоциированное с `clicks`.

window.onload = function() {
  !localStorage.getItem('clicks') && writeClicks(0);
  clicks = parseInt(localStorage.getItem('clicks'));
}

// При click по document инкрементируется переменная clicks.
// При нажатии на ссылку она будет записана в localStorage и Cookie.

document.addEventListener('click', () => {
  clicks += 1;
}, false);

// Запись в localStorage и Cookie происходит именно при click на ссылку.
// При этом click по ссылке не регистрируется.

link.addEventListener('click', () => {
  writeClicks(clicks);
  window.open('https://yandex.ru', 'new window', 'location=yes');
}, false);

// P.S Тестирование в Google Chrome требует открытия файла index.html любым
// способом, кроме file:///
// Создание cookie при просмотре документа с использование file:/// почему-то
// запрещено (пишут, что в целях безопасности).
// В Mozilla Firefox такой проблемы не наблюдается.
